## Objects

Question

```ts
{
    id: Number,
    title: String,
    body: String,
    creation: Number,
    score: Number,
    user: User,
    comments: Comment[],
    answers: Answer[]
}
```

User

```ts
{
    id: Number,
    name: String
}
```

Comment

```ts
{
    id: Number,
    body: String
}
```

Answer

```ts
{
    id: Number,
    body: String,
    creation: Number,
    score: Number,
    user: User,
    accepted: Boolean,
    comments: Comment[]
}
```