use actix_web::web::Data;
use actix_web::{middleware as actix_middleware, web, App, HttpServer};
use routes::*;
use sqlx::postgres::PgPoolOptions;
use std::time::Duration;

mod db;
mod middleware;
mod model;
mod routes;
mod websockets;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=debug");
    pretty_env_logger::init();

    let pool = PgPoolOptions::new()
        .max_connections(10)
        .connect_timeout(Duration::from_secs(15))
        .min_connections(1)
        .connect(
            &std::env::var("DATABASE_URL").expect("Expected DATABASE_URL environment variable"),
        )
        .await
        .unwrap();

    let host = std::env::var("STACKOVERFAUX_HOST").unwrap_or_else(|_| String::from("127.0.0.1"));
    let port = std::env::var("STACKOVERFAUX_PORT")
        .unwrap_or_else(|_| String::from("8080"))
        .parse::<u16>()
        .unwrap();

    let server = HttpServer::new(move || {
        App::new()
            .app_data(Data::new(pool.clone()))
            .wrap(actix_middleware::Logger::default())
            .service(
                web::scope("/api/v1")
                    .service(web::resource("/ws").to(websockets::chat_route))
                    .service(create_user)
                    .service(login)
                    .service(get_user_questions)
                    .service(get_user)
                    .service(delete_user)
                    .service(get_frontpage)
                    .service(get_question)
                    .service(
                        web::scope("")
                            .wrap(middleware::TokenValidator)
                            .service(get_own_questions)
                            .service(vote_on_question)
                            .service(remove_own_vote_on_question)
                            .service(ask_question)
                            .service(delete_question)
                            .service(post_answer_comment)
                            .service(post_question_comment)
                            .service(delete_answer_comment)
                            .service(delete_question_comment)
                            .service(post_answer)
                            .service(update_answer)
                            .service(delete_answer)
                            .service(vote_on_answer)
                            .service(remove_own_vote_on_answer),
                    ),
            )
    })
    .bind((host, port))
    .unwrap()
    .run();
    server.await
}
