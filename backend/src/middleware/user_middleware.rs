use std::cell::RefCell;
use std::pin::Pin;
use std::rc::Rc;
use std::task::{Context, Poll};

use actix_service::{Service, Transform};
use actix_web::{dev::ServiceRequest, dev::ServiceResponse, Error, HttpMessage};
use futures::future::{ok, Future, Ready};

pub struct TokenValidator;

impl<S: 'static, B> Transform<S, ServiceRequest> for TokenValidator
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
    B: 'static,
{
    type Response = ServiceResponse<B>;
    type Error = Error;
    type InitError = ();
    type Transform = TokenValidatorMiddleware<S>;
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(TokenValidatorMiddleware {
            service: Rc::new(RefCell::new(service)),
        })
    }
}

pub struct TokenValidatorMiddleware<S> {
    // This is special: We need this to avoid lifetime issues.
    service: Rc<RefCell<S>>,
}

impl<S, B> Service<ServiceRequest> for TokenValidatorMiddleware<S>
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
    S::Future: 'static,
    B: 'static,
{
    type Response = ServiceResponse<B>;
    type Error = Error;
    #[allow(clippy::type_complexity)]
    type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>>>>;

    fn poll_ready(&self, cx: &mut Context) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(cx)
    }

    fn call(&self, req: ServiceRequest) -> Self::Future {
        let svc = self.service.clone();

        Box::pin(async move {
            let token =
                match req.headers().get("Authorization") {
                    Some(t) => t.to_str().unwrap_or_default(),
                    None => return Err(actix_web::error::ErrorUnauthorized(
                        "No token provided. Please include a token in the 'Authentication' header.",
                    )),
                };

            let user_id = match token.parse::<i32>() {
                Ok(uid) => uid,
                Err(_) => {
                    return Err(actix_web::error::ErrorUnauthorized(
                        "Invalid token provided.",
                    ))
                }
            };
            //let logic = extract_logic();
            req.extensions_mut().insert(CallContext(user_id));
            svc.call(req).await
        })
    }
}

#[derive(Clone, Debug)]
pub struct CallContext(pub i32);
