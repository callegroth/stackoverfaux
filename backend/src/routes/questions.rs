use crate::{
    db::{self, QuestionStrategy},
    middleware::CallContext,
    model::{
        HydratedQuestion, QuestionCreateForm, QuestionDeleteId, QuestionVote, Vote, VoteDirection,
        VoteForm, WsMessageInner,
    },
    websockets::{SendMessage, WsChatServer},
};
use actix::SystemService;
use actix_web::{
    delete, get, post,
    web::{Data, Json, Path, ReqData},
    HttpRequest, HttpResponse,
};
use sqlx::PgPool;

fn get_user_id(req: &HttpRequest) -> Option<i32> {
    req.headers()
        .get("Authorization")?
        .to_str()
        .ok()?
        .parse::<i32>()
        .ok()
}

#[get("/questions")]
async fn get_frontpage(pool: Data<PgPool>, req: HttpRequest) -> HttpResponse {
    let user_id = get_user_id(&req).unwrap_or_default();
    match db::fetch_hydrated_questions(&*pool, QuestionStrategy::FrontPage, user_id).await {
        Ok(mut qs) => {
            for q in qs.iter_mut() {
                q.markdownify();
            }
            HttpResponse::Ok().json(&qs)
        }
        Err(_why) => HttpResponse::InternalServerError().finish(),
    }
}

#[get("/questions/{question_id}")]
async fn get_question(
    pool: Data<PgPool>,
    question_id: Path<i32>,
    req: HttpRequest,
) -> HttpResponse {
    let user_id = get_user_id(&req).unwrap_or_default();
    match db::fetch_hydrated_questions(&*pool, QuestionStrategy::Single(*question_id), user_id)
        .await
        .map(|mut q| q.pop())
    {
        Ok(Some(mut q)) => {
            q.markdownify();
            HttpResponse::Ok().json(&q)
        }
        _ => HttpResponse::InternalServerError().finish(),
    }
}

#[post("/questions")]
async fn ask_question(
    pool: Data<PgPool>,
    user_id: ReqData<CallContext>,
    form: Json<QuestionCreateForm>,
) -> HttpResponse {
    match db::ask_question(&*pool, user_id.0, &*form).await {
        Ok(q) => {
            let mut hydrated = HydratedQuestion {
                question: q,
                answers: vec![],
                comments: vec![],
            };
            hydrated.markdownify();
            let msg = WsMessageInner::QuestionCreate(hydrated.clone()).wrap();
            WsChatServer::from_registry().do_send(SendMessage(msg));
            HttpResponse::Ok().json(&hydrated)
        }
        Err(why) => HttpResponse::InternalServerError().body(format!("{:?}", why)),
    }
}

#[post("/questions/{question_id}")]
async fn delete_question(
    pool: Data<PgPool>,
    user_id: ReqData<CallContext>,
    path: Path<i32>,
) -> HttpResponse {
    match db::delete_question(&*pool, user_id.0, *path).await {
        Ok(_) => {
            let msg = WsMessageInner::QuestionDelete(QuestionDeleteId { id: *path }).wrap();
            WsChatServer::from_registry().do_send(SendMessage(msg));
            HttpResponse::NoContent().finish()
        }
        Err(_why) => HttpResponse::InternalServerError().finish(),
    }
}

#[post("/questions/{question_id}/votes")]
async fn vote_on_question(
    pool: Data<PgPool>,
    path: Path<i32>,
    form: Json<VoteForm>,
    user_id: ReqData<CallContext>,
) -> HttpResponse {
    let direction = match form.direction {
        -1 => VoteDirection::Down,
        1 => VoteDirection::Up,
        _ => return HttpResponse::BadRequest().finish(),
    };
    let question_id = *path;
    match db::vote_on_question(&*pool, user_id.0, question_id, direction).await {
        Ok(score) => {
            let msg = WsMessageInner::QuestionVoteUpdate(QuestionVote {
                question_id,
                vote: Vote {
                    user_id: user_id.0,
                    score,
                    direction: form.direction,
                },
            })
            .wrap();
            WsChatServer::from_registry().do_send(SendMessage(msg));
            HttpResponse::NoContent().finish()
        }
        Err(_why) => HttpResponse::InternalServerError().finish(),
    }
}

#[delete("/questions/{question_id}/votes/@me")]
async fn remove_own_vote_on_question(
    pool: Data<PgPool>,
    question_id: Path<i32>,
    user_id: ReqData<CallContext>,
) -> HttpResponse {
    match db::vote_on_question(&*pool, user_id.0, *question_id, VoteDirection::Neutral).await {
        Ok(score) => {
            let msg = WsMessageInner::QuestionVoteUpdate(QuestionVote {
                question_id: *question_id,
                vote: Vote {
                    user_id: user_id.0,
                    score,
                    direction: 0,
                },
            })
            .wrap();
            WsChatServer::from_registry().do_send(SendMessage(msg));
            HttpResponse::NoContent().finish()
        }
        Err(_why) => HttpResponse::InternalServerError().finish(),
    }
}
