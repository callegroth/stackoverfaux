use crate::{
    db,
    middleware::CallContext,
    model::{
        AnswerComment, AnswerCommentDeleteId, CommentForm, QuestionComment,
        QuestionCommentDeleteId, WsMessageInner,
    },
    websockets::{SendMessage, WsChatServer},
};
use actix::SystemService;
use actix_web::{
    delete, post,
    web::{Data, Json, Path, ReqData},
    HttpResponse,
};
use sqlx::PgPool;

#[post("/questions/{question_id}/answers/{answer_id}/comments")]
pub async fn post_answer_comment(
    pool: Data<PgPool>,
    user_id: ReqData<CallContext>,
    form: Json<CommentForm>,
    path: Path<(i32, i32)>,
) -> HttpResponse {
    let (question_id, answer_id) = *path;
    match db::post_answer_comment(&*pool, user_id.0, answer_id, &form.body).await {
        Ok(q) => {
            let msg = WsMessageInner::AnswerCommentCreate(AnswerComment {
                answer_id,
                question_id,
                comment: q.clone(),
            })
            .wrap();
            WsChatServer::from_registry().do_send(SendMessage(msg));
            HttpResponse::Ok().json(&q)
        }
        Err(_why) => HttpResponse::InternalServerError().finish(),
    }
}

#[post("/questions/{question_id}/comments")]
pub async fn post_question_comment(
    pool: Data<PgPool>,
    user_id: ReqData<CallContext>,
    form: Json<CommentForm>,
    path: Path<i32>,
) -> HttpResponse {
    let question_id = *path;
    match db::post_question_comment(&*pool, user_id.0, question_id, &form.body).await {
        Ok(q) => {
            let msg = WsMessageInner::QuestionCommentCreate(QuestionComment {
                question_id,
                comment: q.clone(),
            })
            .wrap();
            WsChatServer::from_registry().do_send(SendMessage(msg));
            HttpResponse::Ok().json(&q)
        }
        Err(why) => HttpResponse::InternalServerError().body(format!("{:?}", why)),
    }
}

#[delete("/questions/{question_id}/answers/{answer_id}/comments/{comment_id}")]
pub async fn delete_answer_comment(
    pool: Data<PgPool>,
    user_id: ReqData<CallContext>,
    path: Path<(i32, i32, i32)>,
) -> HttpResponse {
    let (question_id, answer_id, comment_id) = *path;
    match db::delete_answer_comment(&*pool, user_id.0, comment_id).await {
        Ok(q) => {
            let msg = WsMessageInner::AnswerCommentDelete(AnswerCommentDeleteId {
                question_id,
                answer_id,
                comment_id,
            })
            .wrap();
            WsChatServer::from_registry().do_send(SendMessage(msg));
            HttpResponse::Ok().json(&q)
        }
        Err(_why) => HttpResponse::InternalServerError().finish(),
    }
}

#[delete("/questions/{question_id}/comments/{comment_id}")]
pub async fn delete_question_comment(
    pool: Data<PgPool>,
    user_id: ReqData<CallContext>,
    path: Path<(i32, i32)>,
) -> HttpResponse {
    let (question_id, comment_id) = *path;
    match db::delete_question_comment(&*pool, user_id.0, comment_id).await {
        Ok(q) => {
            let msg = WsMessageInner::QuestionCommentDelete(QuestionCommentDeleteId {
                question_id,
                comment_id,
            })
            .wrap();
            WsChatServer::from_registry().do_send(SendMessage(msg));
            HttpResponse::Ok().json(&q)
        }
        Err(_why) => HttpResponse::InternalServerError().finish(),
    }
}
