use crate::{
    db,
    middleware::CallContext,
    model::{
        AnswerCreateForm, AnswerDeleteId, AnswerUpdateForm, AnswerVote, Vote, VoteDirection,
        VoteForm, WsMessageInner,
    },
    websockets::{SendMessage, WsChatServer},
};
use actix::SystemService;
use actix_web::{
    delete, patch, post,
    web::{Data, Json, Path, ReqData},
    HttpResponse,
};
use sqlx::PgPool;

#[post("/questions/{question_id}/answers")]
pub async fn post_answer(
    pool: Data<PgPool>,
    user_id: ReqData<CallContext>,
    form: Json<AnswerCreateForm>,
    question_id: Path<i32>,
) -> HttpResponse {
    let form = form.into_inner();
    match db::post_answer(&*pool, user_id.0, *question_id, form.body).await {
        Ok(mut q) => {
            q.markdownify();
            let msg = WsMessageInner::AnswerCreate(q.clone()).wrap();
            WsChatServer::from_registry().do_send(SendMessage(msg));
            HttpResponse::Ok().json(&q)
        }
        Err(why) => HttpResponse::InternalServerError().body(format!("{:?}", why)),
    }
}

#[patch("/questions/{question_id}/answers/{answer_id}")]
pub async fn update_answer(
    pool: Data<PgPool>,
    form: Json<AnswerUpdateForm>,
    path: Path<(i32, i32)>,
) -> HttpResponse {
    let (question_id, answer_id) = *path;
    let form = form.into_inner();
    match db::update_answer(&*pool, question_id, answer_id, form.accepted, form.body).await {
        Ok(mut q) => {
            q.markdownify();
            let msg = WsMessageInner::AnswerUpdate(q.clone()).wrap();
            WsChatServer::from_registry().do_send(SendMessage(msg));
            HttpResponse::Ok().json(&q)
        }
        Err(why) => HttpResponse::InternalServerError().body(format!("{:?}", why)),
    }
}

#[delete("/questions/{question_id}/answers/{answer_id}")]
pub async fn delete_answer(
    pool: Data<PgPool>,
    user_id: ReqData<CallContext>,
    path: Path<(i32, i32)>,
) -> HttpResponse {
    let (question_id, answer_id) = *path;
    match db::delete_answer(&*pool, user_id.0, answer_id).await {
        Ok(q) => {
            let msg = WsMessageInner::AnswerDelete(AnswerDeleteId {
                question_id,
                answer_id,
            })
            .wrap();
            WsChatServer::from_registry().do_send(SendMessage(msg));
            HttpResponse::Ok().json(&q)
        }
        Err(_why) => HttpResponse::InternalServerError().finish(),
    }
}

#[post("/questions/{question_id}/answers/{answer_id}/votes")]
pub async fn vote_on_answer(
    pool: Data<PgPool>,
    path: Path<(i32, i32)>,
    form: Json<VoteForm>,
    user_id: ReqData<CallContext>,
) -> HttpResponse {
    let direction = match form.direction {
        -1 => VoteDirection::Down,
        1 => VoteDirection::Up,
        _ => return HttpResponse::BadRequest().finish(),
    };
    let (question_id, answer_id) = *path;
    match db::vote_on_answer(&*pool, user_id.0, answer_id, direction).await {
        Ok(score) => {
            let msg = WsMessageInner::AnswerVoteUpdate(AnswerVote {
                answer_id,
                question_id,
                vote: Vote {
                    direction: form.direction,
                    user_id: user_id.0,
                    score,
                },
            })
            .wrap();
            WsChatServer::from_registry().do_send(SendMessage(msg));
            HttpResponse::NoContent().finish()
        }
        Err(why) => HttpResponse::InternalServerError().body(format!("{:?}", why)),
    }
}

#[delete("/questions/{question_id}/answers/{answer_id}/votes/@me")]
pub async fn remove_own_vote_on_answer(
    pool: Data<PgPool>,
    path: Path<(i32, i32)>,
    user_id: ReqData<CallContext>,
) -> HttpResponse {
    let (question_id, answer_id) = *path;

    match db::vote_on_answer(&*pool, user_id.0, answer_id, VoteDirection::Neutral).await {
        Ok(score) => {
            let msg = WsMessageInner::AnswerVoteUpdate(AnswerVote {
                answer_id,
                question_id,
                vote: Vote {
                    direction: 0,
                    user_id: user_id.0,
                    score,
                },
            })
            .wrap();
            WsChatServer::from_registry().do_send(SendMessage(msg));
            HttpResponse::NoContent().finish()
        }
        Err(_why) => HttpResponse::InternalServerError().finish(),
    }
}
