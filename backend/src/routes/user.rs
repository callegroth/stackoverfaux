use actix_web::{
    delete, get, post,
    web::{Data, Json, Path, ReqData},
    HttpResponse,
};
use sqlx::PgPool;

use crate::{
    db::{self, QuestionStrategy},
    middleware::CallContext,
    model::UserCreationForm,
};

#[post("/users")]
pub async fn create_user(user: Json<UserCreationForm>, pool: Data<PgPool>) -> HttpResponse {
    match db::create_user(&*pool, &user.name).await {
        Ok(u) => HttpResponse::Ok().json(&u),
        Err(_) => HttpResponse::InternalServerError().finish(),
    }
}

#[post("/login")]
pub async fn login(user: Json<UserCreationForm>, pool: Data<PgPool>) -> HttpResponse {
    match db::get_user(&*pool, &user.name).await {
        Ok(u) => HttpResponse::Ok().json(&u),
        Err(_) => HttpResponse::Unauthorized().finish(),
    }
}

#[get("/users/{user_id}/questions")]
pub async fn get_user_questions(pool: Data<PgPool>, user_id: Path<i32>) -> HttpResponse {
    match db::fetch_hydrated_questions(&*pool, QuestionStrategy::User(*user_id), *user_id).await {
        Ok(q) => HttpResponse::Ok().json(&q),
        Err(_why) => HttpResponse::InternalServerError().finish(),
    }
}

#[get("/users/@me/questions")]
pub async fn get_own_questions(pool: Data<PgPool>, user_id: ReqData<CallContext>) -> HttpResponse {
    match db::fetch_hydrated_questions(&*pool, QuestionStrategy::User(user_id.0), user_id.0).await {
        Ok(q) => HttpResponse::Ok().json(&q),
        Err(_why) => HttpResponse::InternalServerError().finish(),
    }
}

#[get("/users/{user_id}")]
pub async fn get_user(user_id: Path<i32>, pool: Data<PgPool>) -> HttpResponse {
    match db::get_user_by_id(&*pool, *user_id).await {
        Ok(r) => HttpResponse::Ok().json(&r),
        Err(_why) => HttpResponse::InternalServerError().finish(),
    }
}

#[delete("/users/{user_id}")]
pub async fn delete_user(user_id: Path<i32>, pool: Data<PgPool>) -> HttpResponse {
    match db::delete_user(&*pool, *user_id).await {
        Ok(_) => HttpResponse::NoContent().finish(),
        Err(_why) => HttpResponse::InternalServerError().finish(),
    }
}
