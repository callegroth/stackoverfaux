use super::{Answer, Comment, User};
use comrak::ComrakOptions;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Question {
    pub id: i32,
    pub title: String,
    pub body: String,
    pub creation: i64,
    pub score: i32,
    pub vote: i32,
    pub user: User,
}

impl Question {
    fn markdownify(&mut self) {
        let mut options = ComrakOptions::default();
        options.render.github_pre_lang = true;
        self.body = comrak::markdown_to_html(&self.body, &options)
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct HydratedQuestion {
    #[serde(flatten)]
    pub question: Question,
    pub comments: Vec<Comment>,
    pub answers: Vec<Answer>,
}

impl HydratedQuestion {
    pub fn markdownify(&mut self) {
        self.question.markdownify();

        for answer in self.answers.iter_mut() {
            answer.markdownify();
        }
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct DatabaseQuestion {
    pub id: i32,
    pub title: String,
    pub body: String,
    pub created_at: i64,
    pub score: i32,
    pub vote: i32,
    pub author_id: i32,
}

#[derive(Deserialize, Serialize)]
pub struct QuestionCreateForm {
    pub title: String,
    pub body: String,
}
