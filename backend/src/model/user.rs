use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Clone, Debug, sqlx::Type)]
pub struct User {
    pub id: i32,
    pub name: String,
}

#[derive(Deserialize, Serialize)]
pub struct UserCreationForm {
    pub name: String,
}
