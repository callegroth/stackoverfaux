use super::{Answer, Comment, HydratedQuestion};

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct WsMessage {
    pub op: u8,
    pub t: String,

    pub d: WsMessageInner,
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct QuestionDeleteId {
    pub id: i32,
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct QuestionCommentDeleteId {
    pub question_id: i32,
    pub comment_id: i32,
}
#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct AnswerCommentDeleteId {
    pub question_id: i32,
    pub comment_id: i32,
    pub answer_id: i32,
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct AnswerDeleteId {
    pub question_id: i32,
    pub answer_id: i32,
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct QuestionVote {
    pub question_id: i32,
    pub vote: Vote,
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct Vote {
    pub user_id: i32,
    pub score: i32,
    pub direction: i8,
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct AnswerVote {
    pub answer_id: i32,
    pub question_id: i32,
    pub vote: Vote,
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct QuestionComment {
    pub question_id: i32,
    pub comment: Comment,
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct AnswerComment {
    pub question_id: i32,
    pub answer_id: i32,
    pub comment: Comment,
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
#[serde(untagged)]
pub enum WsMessageInner {
    QuestionCreate(HydratedQuestion),
    QuestionUpdate(HydratedQuestion),
    QuestionDelete(QuestionDeleteId),
    AnswerCreate(Answer),
    AnswerUpdate(Answer),
    AnswerDelete(AnswerDeleteId),
    AnswerCommentCreate(AnswerComment),
    AnswerCommentUpdate(AnswerComment),
    AnswerCommentDelete(AnswerCommentDeleteId),
    QuestionCommentCreate(QuestionComment),
    QuestionCommentUpdate(QuestionComment),
    QuestionCommentDelete(QuestionCommentDeleteId),
    QuestionVoteUpdate(QuestionVote),
    AnswerVoteUpdate(AnswerVote),
}

impl WsMessageInner {
    pub fn event_name(&self) -> &'static str {
        match self {
            WsMessageInner::QuestionCreate(_) => "QUESTION_CREATE",
            WsMessageInner::QuestionUpdate(_) => "QUESTION_UPDATE",
            WsMessageInner::QuestionDelete(_) => "QUESTION_DELETE",
            WsMessageInner::AnswerCreate(_) => "ANSWER_CREATE",
            WsMessageInner::AnswerUpdate(_) => "ANSWER_UPDATE",
            WsMessageInner::AnswerDelete(_) => "ANSWER_DELETE",
            WsMessageInner::AnswerCommentCreate(_) => "ANSWER_COMMENT_CREATE",
            WsMessageInner::AnswerCommentUpdate(_) => "ANSWER_COMMENT_UPDATE",
            WsMessageInner::AnswerCommentDelete(_) => "ANSWER_COMMENT_DELETE",
            WsMessageInner::QuestionCommentCreate(_) => "QUESTION_COMMENT_CREATE",
            WsMessageInner::QuestionCommentUpdate(_) => "QUESTION_COMMENT_UPDATE",
            WsMessageInner::QuestionCommentDelete(_) => "QUESTION_COMMENT_DELETE",
            WsMessageInner::QuestionVoteUpdate(_) => "QUESTION_VOTE_UPDATE",
            WsMessageInner::AnswerVoteUpdate(_) => "ANSWER_VOTE_UPDATE",
        }
    }
    pub fn wrap(self) -> WsMessage {
        let t = self.event_name();
        WsMessage {
            t: t.to_string(),
            op: 0,
            d: self,
        }
    }
}
