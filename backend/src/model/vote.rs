use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Clone, Debug)]
pub enum VoteDirection {
    Up,
    Neutral,
    Down,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct VoteForm {
    pub direction: i8,
}
