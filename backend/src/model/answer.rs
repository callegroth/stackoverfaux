use super::{Comment, User};
use comrak::ComrakOptions;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Answer {
    pub id: i32,
    pub body: String,
    pub creation: i64,
    pub score: i32,
    pub user: User,
    pub accepted: bool,
    pub comments: Vec<Comment>,
    pub vote: i32,
    pub question_id: i32,
}

impl Answer {
    pub fn markdownify(&mut self) {
        let mut options = ComrakOptions::default();
        options.render.github_pre_lang = true;
        self.body = comrak::markdown_to_html(&self.body, &options)
    }
}

#[derive(Debug)]
pub struct DatabaseAnswer {
    pub id: i32,
    pub body: String,
    pub creation: i64,
    pub author_id: i32,
    pub accepted: bool,
    pub score: i32,
    pub vote: i32,
    pub question_id: i32,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct AnswerCreateForm {
    pub body: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct AnswerUpdateForm {
    pub body: Option<String>,
    pub accepted: Option<bool>,
}
