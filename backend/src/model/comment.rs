use serde::{Deserialize, Serialize};

use super::User;

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Comment {
    pub id: i32,
    pub body: String,
    pub user: User,
}

#[derive(Debug)]
pub struct DatabaseComment {
    pub id: i32,
    pub body: String,
    pub author_id: i32,
    // This model is used for both answer and question comments
    // so parent_id can be a question_id or an answer_id
    pub parent_id: i32,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct CommentForm {
    pub body: String,
}
