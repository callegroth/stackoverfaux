use actix::prelude::*;
use actix_web_actors::ws;
use log::{debug, info};

use super::message::{ChatMessage, JoinRoom};
use super::server::WsChatServer;

#[derive(Default)]
pub struct WsChatSession {
    id: usize,
}

impl Actor for WsChatSession {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        WsChatServer::from_registry().do_send(JoinRoom(ctx.address().recipient()))
    }

    fn stopped(&mut self, _ctx: &mut Self::Context) {
        info!("WsChatSession closed for {}", self.id,);
    }
}

impl Handler<ChatMessage> for WsChatSession {
    type Result = ();

    fn handle(&mut self, msg: ChatMessage, ctx: &mut Self::Context) {
        ctx.text(msg.0);
    }
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for WsChatSession {
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        let msg = match msg {
            Err(_) => {
                ctx.stop();
                return;
            }
            Ok(msg) => msg,
        };

        debug!("WEBSOCKET MESSAGE: {:?}", msg);
    }
}
