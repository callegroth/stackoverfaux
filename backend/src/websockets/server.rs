use super::message::{ChatMessage, JoinRoom, SendMessage};
use actix::prelude::*;
use actix_broker::BrokerSubscribe;

type Client = Recipient<ChatMessage>;

#[derive(Default)]
pub struct WsChatServer {
    sessions: Vec<Client>,
}

impl WsChatServer {
    fn add_client_to_room(&mut self, client: Client) {
        self.sessions.push(client);
    }
}

impl Actor for WsChatServer {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.subscribe_system_async::<SendMessage>(ctx);
    }
}

impl Handler<JoinRoom> for WsChatServer {
    type Result = ();

    fn handle(&mut self, msg: JoinRoom, _ctx: &mut Self::Context) -> Self::Result {
        let JoinRoom(client) = msg;

        self.add_client_to_room(client);
    }
}

impl Handler<SendMessage> for WsChatServer {
    type Result = ();

    fn handle(&mut self, msg: SendMessage, _ctx: &mut Self::Context) {
        let msg = ChatMessage(serde_json::to_string(&msg.0).unwrap());

        for s in self.sessions.iter() {
            let _ = s.do_send(msg.clone());
        }
    }
}

impl SystemService for WsChatServer {}
impl Supervised for WsChatServer {}
