use sqlx::PgPool;

use crate::model::{Answer, User};

pub async fn post_answer(
    pool: &PgPool,
    user_id: i32,
    question_id: i32,
    body: String,
) -> sqlx::Result<Answer> {
    let res = sqlx::query!(
        r#"WITH inserted AS (
            INSERT INTO answers(author_id, question_id, body)
            VALUES ($1, $2, $3)
            RETURNING *
        )
        SELECT inserted.id as "id!"
            , users.id as "user_id!"
            , users.name as "name!"
            , EXTRACT(epoch FROM created_at)::BIGINT AS "creation!"
        FROM inserted
        INNER JOIN users ON users.id = inserted.author_id"#,
        user_id,
        question_id,
        &body
    )
    .fetch_one(pool)
    .await?;
    Ok(Answer {
        id: res.id,
        body,
        creation: res.creation,
        score: 0,
        user: User {
            id: res.user_id,
            name: res.name,
        },
        accepted: false,
        comments: vec![],
        vote: 0,
        question_id,
    })
}

pub async fn update_answer(
    pool: &PgPool,
    question_id: i32,
    answer_id: i32,
    accept: Option<bool>,
    body: Option<String>,
) -> sqlx::Result<Answer> {
    let res = sqlx::query!(
        r#"WITH updated AS (
            UPDATE answers
            SET 
                accepted_at = 
                    CASE WHEN $1::BOOLEAN IS NULL THEN accepted_at
                        WHEN $1::BOOLEAN = TRUE THEN CURRENT_TIMESTAMP
                        ELSE NULL
                    END,
                body = 
                    CASE WHEN $2::TEXT IS NULL THEN body
                        ELSE $2::TEXT
                    END
            WHERE id=$3 AND question_id=$4
            RETURNING *
        )
        SELECT updated.id as "id!"
            , users.id as "user_id!"
            , users.name as "name!"
            , EXTRACT(epoch FROM created_at)::BIGINT AS "creation!"
            , body as "body!"
            , accepted_at is not null as "accepted!"
        FROM updated
        INNER JOIN users ON users.id = updated.author_id"#,
        accept,
        body.as_deref(),
        answer_id,
        question_id
    )
    .fetch_one(pool)
    .await?;
    Ok(Answer {
        id: res.id,
        body: res.body,
        creation: res.creation,
        score: 0,
        user: User {
            id: res.user_id,
            name: res.name,
        },
        accepted: res.accepted,
        comments: vec![],
        vote: 0,
        question_id,
    })
}

pub async fn delete_answer(pool: &PgPool, user_id: i32, answer_id: i32) -> sqlx::Result<()> {
    sqlx::query!(
        "DELETE FROM answers
        WHERE id=$1 AND author_id=$2",
        answer_id,
        user_id
    )
    .fetch_one(pool)
    .await?;
    Ok(())
}
