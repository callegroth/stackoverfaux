use crate::model::User;
use sqlx::PgPool;

pub async fn create_user(pool: &PgPool, name: &str) -> sqlx::Result<User> {
    sqlx::query_as!(
        User,
        "INSERT INTO users(name) 
         VALUES ($1) 
         RETURNING *",
        name
    )
    .fetch_one(pool)
    .await
}

pub async fn get_user(pool: &PgPool, name: &str) -> sqlx::Result<User> {
    sqlx::query_as!(User, "SELECT name, id FROM users WHERE name=$1", name)
        .fetch_one(pool)
        .await
}

pub async fn get_user_by_id(pool: &PgPool, id: i32) -> sqlx::Result<User> {
    sqlx::query_as!(User, "SELECT name, id FROM users WHERE id=$1", id)
        .fetch_one(pool)
        .await
}

pub async fn delete_user(pool: &PgPool, user_id: i32) -> sqlx::Result<()> {
    sqlx::query!("DELETE FROM users WHERE id=$1;", user_id)
        .execute(pool)
        .await?;
    Ok(())
}
