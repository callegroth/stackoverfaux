use crate::model::{
    Answer, Comment, DatabaseAnswer, DatabaseComment, DatabaseQuestion, HydratedQuestion, Question,
    QuestionCreateForm, User,
};
use sqlx::PgPool;
use std::collections::{HashMap, HashSet};

pub enum QuestionStrategy {
    FrontPage,
    User(i32),
    Single(i32),
}

pub async fn ask_question(
    pool: &PgPool,
    user_id: i32,
    form: &QuestionCreateForm,
) -> sqlx::Result<Question> {
    sqlx::query_as!(
        Question,
        r#"WITH inserted AS (
            INSERT INTO questions (author_id, title, body)
            VALUES($1, $2, $3)
            RETURNING id
                    , title
                    , body
                    , EXTRACT(epoch FROM created_at)::BIGINT AS "creation!"
                    , 0 as "score!"
                    , author_id
        )
        SELECT inserted.id as "id!"
            , title as "title!"
            , body as "body!"
            , "creation!"
            , "score!"
            , (author_id, name) as "user!:User"
            , 0 AS "vote!"
        FROM inserted
        INNER JOIN users ON users.id = author_id;"#,
        user_id,
        form.title,
        form.body
    )
    .fetch_one(pool)
    .await
}

pub async fn delete_question(pool: &PgPool, user_id: i32, question_id: i32) -> sqlx::Result<()> {
    sqlx::query_as!(
        DatabaseQuestion,
        "DELETE FROM questions
         WHERE id=$1 AND author_id=$2;",
        question_id,
        user_id
    )
    .execute(pool)
    .await?;
    Ok(())
}

pub async fn fetch_hydrated_questions(
    pool: &PgPool,
    strategy: QuestionStrategy,
    user_id: i32,
) -> sqlx::Result<Vec<HydratedQuestion>> {
    // Initially I had a query with 9 left joins
    // But it was impossible to work with

    // First we get the actual questions
    let questions = match strategy {
        QuestionStrategy::FrontPage => fetch_questions(pool, user_id).await?,
        QuestionStrategy::User(user_id) => fetch_user_questions(pool, user_id).await?,
        QuestionStrategy::Single(question_id) => {
            fetch_single_question(pool, question_id, user_id).await?
        }
    };

    let question_ids: Vec<i32> = questions.iter().map(|row| row.id).collect();
    // then we get the comments directly to the questions
    let question_comments = fetch_question_comments(pool, &question_ids).await?;

    // Next step is to get the answers to our questions
    // This query is arguably simple enough that a join on the comments
    // to the questions is a reasonable thing to put in one query
    // but for consistency I chose not to
    let answers = fetch_answers(pool, &question_ids, user_id).await?;

    // Almost done, next step is fetching the comments on the answers we just saw
    let answer_ids: Vec<i32> = answers.iter().map(|row| row.id).collect();
    let answer_comments = fetch_answer_comments(pool, &answer_ids).await?;

    // Last step is collecting all the user ids and fetching the user objects from it
    let question_users = questions.iter().map(|row| row.author_id);
    let comment_users = question_comments.iter().map(|row| row.author_id);
    let answer_users = answers.iter().map(|row| row.author_id);
    let answer_comment_users = answer_comments.iter().map(|row| row.author_id);

    let user_ids: HashSet<i32> = question_users
        .chain(comment_users)
        .chain(answer_users)
        .chain(answer_comment_users)
        .collect();
    let user_ids: Vec<i32> = user_ids.into_iter().collect();
    let users = fetch_users(pool, &user_ids).await?;

    let hydrated = hydrate_questions(
        users,
        answer_comments,
        question_comments,
        answers,
        questions,
    );

    Ok(hydrated)
}

pub async fn fetch_questions(pool: &PgPool, user_id: i32) -> sqlx::Result<Vec<DatabaseQuestion>> {
    sqlx::query_as!(
        DatabaseQuestion,
        r#"
        SELECT q.id as "id!"
             , q.title
             , q.body
             , EXTRACT(epoch FROM q.created_at)::BIGINT AS "created_at!"
             , COALESCE(quv.votes, 0)::INTEGER AS "score!"
             , q.author_id
             , COALESCE(vote, 0)::INTEGER AS "vote!"
        FROM questions q 
        INNER JOIN users qa ON q.author_id = qa.id
        LEFT JOIN question_upvotes quv ON quv.id = q.id
        LEFT JOIN question_user_votes votes ON votes.question_id = q.id AND votes.author_id=$1
        ORDER BY q.created_at DESC
        LIMIT 25
        "#,
        user_id
    )
    .fetch_all(pool)
    .await
}

pub async fn fetch_user_questions(
    pool: &PgPool,
    user_id: i32,
) -> sqlx::Result<Vec<DatabaseQuestion>> {
    sqlx::query_as!(
        DatabaseQuestion,
        r#"
        SELECT q.id as "id!"
             , q.title
             , q.body
             , EXTRACT(epoch FROM q.created_at)::BIGINT AS "created_at!"
             , COALESCE(quv.votes, 0)::INTEGER AS "score!"
             , q.author_id
             , COALESCE(vote, 0)::INTEGER AS "vote!"
        FROM questions q 
        INNER JOIN users qa ON q.author_id = qa.id
        LEFT JOIN question_upvotes quv ON quv.id = q.id
        LEFT JOIN question_user_votes votes ON votes.question_id = q.id AND votes.author_id=$1
        WHERE q.author_id = $1
        ORDER BY q.created_at DESC
        LIMIT 25
        "#,
        user_id
    )
    .fetch_all(pool)
    .await
}

pub async fn fetch_single_question(
    pool: &PgPool,
    question_id: i32,
    user_id: i32,
) -> sqlx::Result<Vec<DatabaseQuestion>> {
    sqlx::query_as!(
        DatabaseQuestion,
        r#"
        SELECT q.id as "id!"
             , q.title
             , q.body
             , EXTRACT(epoch FROM q.created_at)::BIGINT AS "created_at!"
             , COALESCE(quv.votes, 0)::INTEGER AS "score!"
             , q.author_id
             , COALESCE(vote, 0)::INTEGER AS "vote!"
        FROM questions q 
        INNER JOIN users qa ON q.author_id = qa.id
        LEFT JOIN question_upvotes quv ON quv.id = q.id
        LEFT JOIN question_user_votes votes ON votes.question_id = q.id AND votes.author_id=$2
        WHERE q.id = $1
        "#,
        question_id,
        user_id
    )
    .fetch_all(pool)
    .await
}

pub async fn fetch_question_comments(
    pool: &PgPool,
    question_ids: &[i32],
) -> sqlx::Result<Vec<DatabaseComment>> {
    sqlx::query_as!(
        DatabaseComment,
        "SELECT id
             , body
             , author_id
             , parent_id
        FROM question_comments
        WHERE parent_id = ANY($1)",
        question_ids
    )
    .fetch_all(pool)
    .await
}

pub async fn fetch_answers(
    pool: &PgPool,
    question_ids: &[i32],
    user_id: i32,
) -> sqlx::Result<Vec<DatabaseAnswer>> {
    sqlx::query_as!(
        DatabaseAnswer,
        r#"
        SELECT a.id AS "id!"
             , body AS "body!"
             , EXTRACT(epoch FROM created_at)::BIGINT AS "creation!"
             , a.author_id AS "author_id!"
             , accepted_at IS NOT NULL AS "accepted!"
             , question_id AS "question_id!"
             , COALESCE(auv.votes, 0)::INTEGER AS "score!"
             , COALESCE(vote, 0)::INTEGER AS "vote!"
        FROM answers a
        LEFT JOIN answer_upvotes auv ON auv.id = a.id
        LEFT JOIN answer_user_votes votes ON votes.answer_id = a.id AND votes.author_id=$2
        WHERE a.question_id = ANY($1)
        "#,
        question_ids,
        user_id
    )
    .fetch_all(pool)
    .await
}

pub async fn fetch_answer_comments(
    pool: &PgPool,
    answer_ids: &[i32],
) -> sqlx::Result<Vec<DatabaseComment>> {
    sqlx::query_as!(
        DatabaseComment,
        "SELECT id
             , body
             , author_id
             , parent_id
        FROM answer_comments
        WHERE parent_id = ANY($1)",
        answer_ids
    )
    .fetch_all(pool)
    .await
}

pub async fn fetch_users(pool: &PgPool, user_ids: &[i32]) -> sqlx::Result<Vec<User>> {
    sqlx::query_as!(
        User,
        "
        SELECT id
             , name
        FROM users
        WHERE id = ANY($1)
        ",
        &user_ids
    )
    .fetch_all(pool)
    .await
}

fn hydrate_questions(
    users: Vec<User>,
    answer_comments: Vec<DatabaseComment>,
    question_comments: Vec<DatabaseComment>,
    answers: Vec<DatabaseAnswer>,
    questions: Vec<DatabaseQuestion>,
) -> Vec<HydratedQuestion> {
    let users: HashMap<i32, User> = users.into_iter().map(|user| (user.id, user)).collect();

    // We will create hashmaps for our other entities too, except slightly differently
    let mut answer_comment_map = HashMap::new();
    for ac in answer_comments {
        let user = match users.get(&ac.author_id) {
            Some(user) => user,
            None => continue,
        };
        answer_comment_map
            .entry(ac.parent_id)
            .or_insert_with(Vec::new)
            .push(Comment {
                id: ac.id,
                body: ac.body,
                user: (*user).clone(),
            })
    }

    let mut question_comment_map = HashMap::new();
    for qc in question_comments {
        let user = match users.get(&qc.author_id) {
            Some(user) => user,
            None => continue,
        };
        question_comment_map
            .entry(qc.parent_id)
            .or_insert_with(Vec::new)
            .push(Comment {
                id: qc.id,
                body: qc.body,
                user: (*user).clone(),
            })
    }

    let mut hydrated_answers: HashMap<i32, Vec<Answer>> = HashMap::new();
    for answer in answers {
        let user = match users.get(&answer.author_id) {
            Some(user) => user,
            None => {
                continue;
            }
        };
        // Why remove over get?
        // We only need it once, and remove gives us the actual object rather than a reference
        // to it, saving a clone!
        let comments = answer_comment_map.remove(&answer.id).unwrap_or_default();
        hydrated_answers
            .entry(answer.question_id)
            .or_insert_with(Vec::new)
            .push(Answer {
                id: answer.id,
                body: answer.body,
                creation: answer.creation,
                score: answer.score,
                user: (*user).clone(),
                accepted: answer.accepted,
                vote: answer.vote,
                question_id: answer.question_id,
                comments,
            })
    }

    // Okay, good lord. Finally getting there
    // Last step is to create some hydrated questions
    // with techniques shown before
    let mut hydrated_questions = Vec::new();
    for question in questions {
        let user = match users.get(&question.author_id) {
            Some(user) => user,
            None => {
                continue;
            }
        };
        let mut comments = question_comment_map
            .remove(&question.id)
            .unwrap_or_default();
        let mut answers = hydrated_answers.remove(&question.id).unwrap_or_default();
        answers.sort_by_key(|k| k.id);
        comments.sort_by_key(|k| k.id);
        hydrated_questions.push(HydratedQuestion {
            question: Question {
                id: question.id,
                title: question.title,
                body: question.body,
                creation: question.created_at,
                score: question.score,
                user: (*user).clone(),
                vote: question.vote,
            },
            comments,
            answers,
        })
    }
    hydrated_questions
}
