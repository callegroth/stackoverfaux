use crate::model::VoteDirection;
use sqlx::PgPool;

pub async fn vote_on_question(
    pool: &PgPool,
    user_id: i32,
    question_id: i32,
    direction: VoteDirection,
) -> sqlx::Result<i32> {
    let direction = match direction {
        VoteDirection::Up => 1,
        VoteDirection::Down => -1,
        VoteDirection::Neutral => {
            sqlx::query!(
                "DELETE FROM question_user_votes WHERE question_id=$1 AND author_id=$2;",
                question_id,
                user_id
            )
            .execute(pool)
            .await?;
            return get_question_votes(pool, question_id).await;
        }
    };

    sqlx::query!(
        "INSERT INTO question_user_votes (question_id, author_id, vote)
            VALUES ($1, $2, $3)
            ON CONFLICT (question_id, author_id)
            DO UPDATE set vote = EXCLUDED.vote
        ",
        question_id,
        user_id,
        direction
    )
    .execute(pool)
    .await?;
    get_question_votes(pool, question_id).await
}

pub async fn get_question_votes(pool: &PgPool, question_id: i32) -> sqlx::Result<i32> {
    let res = sqlx::query!(
        r#"SELECT COALESCE(votes, 0)::INTEGER as "score!" FROM question_upvotes WHERE id=$1"#,
        question_id
    )
    .fetch_optional(pool)
    .await?;
    match res {
        Some(res) => Ok(res.score),
        None => Ok(0),
    }
}

pub async fn get_answer_votes(pool: &PgPool, answer_id: i32) -> sqlx::Result<i32> {
    let res = sqlx::query!(
        r#"SELECT COALESCE(votes, 0)::INTEGER as "score!" FROM answer_upvotes WHERE id=$1"#,
        answer_id
    )
    .fetch_optional(pool)
    .await?;
    match res {
        Some(res) => Ok(res.score),
        None => Ok(0),
    }
}

pub async fn vote_on_answer(
    pool: &PgPool,
    user_id: i32,
    answer_id: i32,
    direction: VoteDirection,
) -> sqlx::Result<i32> {
    let direction = match direction {
        VoteDirection::Up => 1,
        VoteDirection::Down => -1,
        VoteDirection::Neutral => {
            sqlx::query!(
                "DELETE FROM answer_user_votes
                 WHERE answer_id=$1 AND author_id=$2;",
                answer_id,
                user_id
            )
            .execute(pool)
            .await?;
            return get_answer_votes(pool, answer_id).await;
        }
    };

    sqlx::query!(
        "INSERT INTO answer_user_votes (answer_id, author_id, vote)
            VALUES ($1, $2, $3)
            ON CONFLICT (answer_id, author_id)
            DO UPDATE set vote = EXCLUDED.vote",
        answer_id,
        user_id,
        direction
    )
    .execute(pool)
    .await?;
    get_answer_votes(pool, answer_id).await
}
