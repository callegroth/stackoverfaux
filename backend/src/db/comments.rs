use sqlx::PgPool;

use crate::model::{Comment, User};

pub async fn post_answer_comment(
    pool: &PgPool,
    user_id: i32,
    answer_id: i32,
    body: &str,
) -> sqlx::Result<Comment> {
    let res = sqlx::query_as!(
        Comment,
        r#"
        WITH inserted AS (
            INSERT INTO answer_comments (author_id, parent_id, body)
            VALUES ($1, $2, $3)
            RETURNING id, body, author_id
        )
        SELECT inserted.id as "id!"
            , body as "body!"
            , (author_id, name) as "user!:User"
        FROM inserted
        INNER JOIN users ON users.id = author_id;"#,
        user_id,
        answer_id,
        body
    )
    .fetch_one(pool)
    .await?;
    Ok(res)
}

pub async fn post_question_comment(
    pool: &PgPool,
    user_id: i32,
    question_id: i32,
    body: &str,
) -> sqlx::Result<Comment> {
    let res = sqlx::query_as!(
        Comment,
        r#"
        WITH inserted AS (
            INSERT INTO question_comments (author_id, parent_id, body)
            VALUES ($1, $2, $3)
            RETURNING id, body, author_id
        )
        SELECT inserted.id as "id!"
            , body as "body!"
            , (author_id, name) as "user!:User"
        FROM inserted
        INNER JOIN users ON users.id = author_id;"#,
        user_id,
        question_id,
        body
    )
    .fetch_one(pool)
    .await?;
    Ok(res)
}

pub async fn delete_answer_comment(
    pool: &PgPool,
    user_id: i32,
    comment_id: i32,
) -> sqlx::Result<()> {
    sqlx::query!(
        "DELETE FROM answer_comments
        WHERE id=$1 AND author_id=$2",
        comment_id,
        user_id
    )
    .fetch_one(pool)
    .await?;
    Ok(())
}

pub async fn delete_question_comment(
    pool: &PgPool,
    user_id: i32,
    comment_id: i32,
) -> sqlx::Result<()> {
    sqlx::query!(
        "DELETE FROM question_comments
        WHERE id=$1 AND author_id=$2",
        comment_id,
        user_id
    )
    .fetch_one(pool)
    .await?;
    Ok(())
}
