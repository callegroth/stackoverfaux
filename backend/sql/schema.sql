CREATE TABLE IF NOT EXISTS users (
	id SERIAL PRIMARY KEY,
	name TEXT NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS uniq_user_name ON users(name);

CREATE TABLE IF NOT EXISTS questions (
	id SERIAL PRIMARY KEY,
	title TEXT NOT NULL,
	body TEXT NOT NULL,
	created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (NOW() AT TIME ZONE 'utc'),
	author_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS answers (
	id SERIAL PRIMARY KEY,
	body TEXT NOT NULL,
	created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (NOW() AT TIME ZONE 'utc'),
	author_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
	question_id INTEGER NOT NULL REFERENCES questions(id) ON DELETE CASCADE,
	accepted_at TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE IF NOT EXISTS question_comments (
	id SERIAL PRIMARY KEY,
	body TEXT NOT NULL,
	created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (NOW() AT TIME ZONE 'utc'),
	author_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
	parent_id INTEGER NOT NULL REFERENCES questions(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS answer_comments (
	id SERIAL PRIMARY KEY,
	body TEXT NOT NULL,
	created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (NOW() AT TIME ZONE 'utc'),
	author_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
	parent_id INTEGER NOT NULL REFERENCES answers(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS question_user_votes (
	question_id INTEGER NOT NULL REFERENCES questions(id) ON DELETE CASCADE,
	author_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
	vote SMALLINT NOT NULL CHECK (vote = 1 OR vote = -1),
	PRIMARY KEY (question_id, author_id)
);

CREATE TABLE IF NOT EXISTS answer_user_votes (
	answer_id INTEGER NOT NULL REFERENCES answers(id) ON DELETE CASCADE,
	author_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
	vote SMALLINT NOT NULL CHECK (vote = 1 OR vote = -1),
	PRIMARY KEY (answer_id, author_id)
);


CREATE MATERIALIZED VIEW IF NOT EXISTS question_upvotes AS
SELECT q.id, SUM(v.vote) AS votes
FROM questions q
INNER JOIN question_user_votes v ON q.id = v.question_id
GROUP BY q.id;

CREATE MATERIALIZED VIEW IF NOT EXISTS answer_upvotes AS
SELECT a.id, SUM(v.vote) AS votes
FROM answers a
INNER JOIN answer_user_votes v ON a.id = v.answer_id
GROUP BY a.id;

CREATE OR REPLACE FUNCTION refresh_question_view() RETURNS TRIGGER LANGUAGE plpgsql
AS
$$
BEGIN
REFRESH MATERIALIZED VIEW question_upvotes;
RETURN NULL;
END
$$;
DROP TRIGGER IF EXISTS question_user_votes_trigger ON question_user_votes;
CREATE TRIGGER question_user_votes_trigger
AFTER INSERT OR UPDATE OR DELETE
ON question_user_votes
FOR EACH ROW
EXECUTE PROCEDURE refresh_question_view();

CREATE OR REPLACE FUNCTION refresh_answer_view() RETURNS TRIGGER LANGUAGE plpgsql
AS
$$
BEGIN
REFRESH MATERIALIZED VIEW answer_upvotes;
RETURN NULL;
END
$$;
DROP TRIGGER IF EXISTS answer_user_votes_trigger ON answer_user_votes;
CREATE TRIGGER answer_user_votes_trigger
AFTER INSERT OR UPDATE OR DELETE
ON answer_user_votes
FOR EACH ROW
EXECUTE PROCEDURE refresh_answer_view();