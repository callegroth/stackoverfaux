import Vue from 'vue';
import Router from 'vue-router';

const Frontpage = () => import('@/components/Frontpage')
const Container = () => import('@/components/Container')
const Login = () => import('@/components/Login')
const Register = () => import('@/components/Register')
const CreateQuestion = () => import('@/components/CreateQuestion')
const QuestionAnswerPage = () => import('@/components/QuestionAnswerPage')
const UserPage = () => import('@/components/UserPage')

Vue.use(Router);

export default new Router({
    mode: "history",
    linkActiveClass: 'open active',
    routes: [
        {
            path: '/',
            name: "Container",
            component: Container,
            children: [
                {
                    path: '',
                    name: 'Frontpage',
                    component: Frontpage,
                },
                {
                    path: 'login',
                    name: 'Login',
                    component: Login
                },
                {
                    path: 'register',
                    name: 'Register',
                    component: Register
                },
                {
                    path: 'ask',
                    name: 'CreateQuestion',
                    component: CreateQuestion
                },
                {
                    path: 'questions/:question_id(\\d+)',
                    name: 'QuestionAnswerPage',
                    component: QuestionAnswerPage
                },
                {
                    path: 'users/:user_id(\\d+)',
                    name: 'Userpage',
                    component: UserPage
                },
            ]
        }
    ]
})