import miniToastr from 'mini-toastr'

miniToastr.init()

function toast({ title, message, type, timeout, cb }) {
    return miniToastr[type](message, title, timeout, cb)
}

export default {
    success: toast,
    error: toast,
    info: toast,
    warn: toast
}

