import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
    storage: window.localStorage
})

export default new Vuex.Store({
    state: {
        user: {},
        questions: {}
    },
    mutations: {
        login(state, obj) {
            state.user = obj
        },
        logout(state) {
            state.user = {}
        },
        handleQuestionVote(state, obj) {
            const { question_id, vote } = obj;
            const { user_id, score, direction } = vote;
            const maybe_question = state.questions[question_id];
            if (maybe_question) {
                if (user_id === state.user.id) {
                    Vue.set(maybe_question, "vote", direction);
                }
                Vue.set(maybe_question, "score", score);
            }
        },
        handleAnswerVote(state, obj) {
            const { question_id, answer_id, vote } = obj;
            const { user_id, score, direction } = vote;
            const maybe_question = state.questions[question_id];
            if (maybe_question) {
                const maybe_answer = maybe_question.answers.find((a) => a.id === answer_id);
                if (maybe_answer) {
                    if (user_id === state.user.id) {
                        Vue.set(maybe_answer, "vote", direction);
                    }
                    Vue.set(maybe_answer, "score", score);
                }
            }
        },
        handleAccepted(state, obj) {
            const { question_id, answer_id, accepted } = obj;
            const maybe_question = state.questions[question_id];
            if (maybe_question) {
                let maybe_answer = maybe_question.answers.find((a) => a.id == answer_id);
                if (maybe_answer) {
                    Vue.set(maybe_answer, "accepted", accepted);
                }
            }
        },
        handleAnswer(state, answer) {
            let question = state.questions[answer.question_id];
            let maybe_answer = question.answers.find((a) => a.id === answer.id);
            if (maybe_answer) {
                if (answer.comments.length < maybe_answer.comments.length) {
                    // Ugly hack to prevent update events
                    // from overwriting comments
                    answer.comments = maybe_answer.comments;
                }

                maybe_answer = Object.assign({}, maybe_answer, answer);
                return;
            }
            question.answers.push(answer);
        },
        handleAnswerDelete(state, ev) {
            let question = state.questions[ev.question_id];
            for (let i = 0; i < question.answers.length; i++) {
                let a = question.answers[i];
                if (a.id === ev.answer_id) {
                    question.answers.splice(i, 1);
                }
            }
        },
        handleAnswerComment(state, obj) {
            const { question_id, answer_id, comment } = obj;
            const question = state.questions[question_id];
            const answer = question.answers.find((a) => a.id == answer_id)
            if (answer) {
                const maybe_comment = answer.comments.find((c) => c.id === comment.id);
                if (maybe_comment) {
                    Vue.set(maybe_comment, "body", comment.body);
                } else {
                    answer.comments.push(comment);
                }
            }
        },
        handleQuestionComment(state, obj) {
            const { question_id, comment } = obj;
            let question = state.questions[question_id];
            const maybe_comment = question.comments.find((c) => c.id === comment.id);
            if (maybe_comment) {
                Vue.set(maybe_comment, "body", comment.body);
            } else {
                question.comments.push(comment);
            }
        },
        handleQuestion(state, question) {
            Vue.set(state.questions, question.id, question);
        },
        handleQuestionDelete(state, ev) {
            Vue.delete(state.questions, ev.id);
        },
        handleQuestions(state, questions) {
            state.questions = {};
            for (let question of questions) {
                state.questions[question.id] = question;
            }
        }
    },
    plugins: [vuexLocal.plugin]
})