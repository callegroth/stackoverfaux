import Vue from 'vue'
import App from './App.vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import router from './router/index';
import store from './vuex/store'
import options from './toasts';
import VueNotifications from 'vue-notifications'


Vue.use(Buefy)
Vue.use(VueNotifications, options)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
