# Stackoverfaux

Stackoverfaux is a clone of the popular website StackOverflow. It consists of a front-end written in vue.js and a backend written in Rust using actix-web. The database used is Postgres.

## Building the webserver

Navigate to the backend folder and simply run
```bash
$ docker build .
```

## Running the whole stack

Simply run 
```bash 
$ docker-compose up -d
```
in the root directory

## API Documentation

Work in progress, but go to `backend/docs`

## Cool features I'm proud of

Websockets! Whenever a change is made, that state change is reflected via WebSockets. It is *very* simple (every client receives every update) but the events themselves are mostly robust.

The front-end assigns a random avatar that sticks using a somewhat clever design.

The database schema uses foreign keys to such an extent that the problem 

> delete a user and all their questions/answers/comments

Is literally
```sql
DELETE FROM users WHERE id=$1;
```

In general, I am happy with how the schema ended up. The fact that comments and votes come in two different flavors despite being structurally identical hurt at first ("had to" because of foreign keys). But actually working with the schema, I believe this ended up being a good choice. 

Sane, sort of obvious design choices I made: 
* created_at being a timestamp and not unix
* accepted_at not just being a bool (but rather using the nullability as a pseudo-bool). That said, I think accepted_at could arguably be its own table with a (question_id, answer_id) PK, a non-nullable timestamp, and a user_id column. This would allow for the behavior StackOverflow actually displays today
* storing body as markdown and transforming that to HTML
* storing votes individually


I understand the problem literally said 

> no need to implement all of them

But I'm honestly pretty happy I did, it ended up being surprisingly tricky at times.

I think the API verbs and endpoints are nicely structured.


## Project layout

### Backend

* model
  - Contains structs and some of their utility functions
* routes
  - Contains the actual route handlers
* middlewares
  - Contains the one middleware used to extract the user id of the person making the request
* errors
  - Contains nothing because I do not have error handling
* db
  - Contains the database queries and some business logic to hydrate the structs
* websockets
  - Contains the route handler and business logic for websockets

For a typical "authorized" request, it basically goes:

Request comes in, actix finds the relevant handler

* the middleware extracts the user id

* the handler extracts some information from the request required to fulfill it

* the handler calls a database function 

* if successful: a websocket message is sent out

* the handler returns the response in json form

## Shortcuts taken

* **Essentially no authorization**. Requests are sent with `Authorization: <user_id>` Oh, and passwords don't exist
* **ANSWER_UPDATE events sent over ws always have empty comments**
* The materialized views for votes are *really* bad at scale. (unless Postgres does some magic I am unaware of)
* No caching
* No searching for questions
* No question tags
* No user mentions
* Questions on the front page are not reactive (I tried!)
* No moderation actions
* No moderation actions [marked as duplicate by Carl Oct 15'21 at 6:18]
* **Basically no validation**
* No syntax highlighting
* Essentially no error handling
* Editing questions/answers(/comments?)
* Websockets assume a single api process exists

## Future improvements
(I'll be vague here in text so I can actually say something in the interview)

* Fixing all the shortcuts

## Usage

## Screenshots

![](https://i.imgur.com/BTrrP78.png)
![](https://i.imgur.com/NpHLnYo.png)
![](https://i.imgur.com/UMqoBHR.png)


## License
[MIT](https://choosealicense.com/licenses/mit/)